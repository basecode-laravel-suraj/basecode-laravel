<!--- Main layout ---->
@extends('backend.layouts.main')

@section('title', CHANGE_PASSWORD_LABEL)

<!--- Breadcrumb ---->
@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">{{ CHANGE_PASSWORD_LABEL }}</li>
@stop

<!--- Page content ---->
@section('content')
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ CHANGE_PASSWORD_LABEL }}</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('backend.change_password') }}" autocomplete="off">
                    {{ csrf_field() }}
                
                    <div class="form-group">
                        <label for="vCurrentPassword">Current password</label>
                        <input type="password" class="form-control" id="vCurrentPassword" name="vCurrentPassword">
                        <span class="text-danger">{{ $errors->first('vCurrentPassword') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="vNewPassword">New password</label>
                        <input type="password" class="form-control" id="vNewPassword" name="vNewPassword">
                        <span class="text-danger">{{ $errors->first('vNewPassword') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="vConfirmPassword">Confirm password</label>
                        <input type="password" class="form-control" id="vConfirmPassword" name="vConfirmPassword">
                        <span class="text-danger">{{ $errors->first('vConfirmPassword') }}</span>
                    </div>
                
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
@stop


