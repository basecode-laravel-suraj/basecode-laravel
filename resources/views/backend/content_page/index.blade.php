@extends('backend.layouts.main')
@section('title', CONTENT_PAGE_LABEL)


@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">{{ CONTENT_PAGE_LABEL }} list</li>
@stop


<!-- Page content --->
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ CONTENT_PAGE_LABEL }}</h3>
            </div>

            <div class="card-body">
                <div class="dt-responsive">
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th style="width: 50px;">#</th>
                                <th>Title</th>
                                <th>Slug</th>
                                <th style="width: 170px;">Updated at</th>
                                <th style="width: 130px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $val)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $val->vPageName }}</td>
                                <td>{{ $val->vSlug }}</td>
                                <td>{{ date('Y-d-m h:i A',$val->iUpdatedAt) }}</td>
                                <td>
                                    <a href="{{ route('backend.content_page.show',['content_page' => $val->vPageUuid]) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('backend.content_page.edit',['content_page' => $val->vPageUuid]) }}" class="btn btn-info btn-xs"><i class="fas fa-edit" data-toggle="tooltip" data-placement="top"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop