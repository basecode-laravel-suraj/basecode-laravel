<?php
use Illuminate\Support\Facades\Auth;
$routeArr = explode(".", app('request')->route()->getAction('as'));
?>
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('backend.home') }}" class="brand-link" style="text-align:center;">
      <img src="{{ asset('theme/img/logo.png') }}" width="150" alt="{{ config('app.name') }}"/>
    </a>

    <div class="text-light" style="border-bottom:1px solid #4B545C;padding: 2px 5px 5px 5px;">
        <small><b>Last login :</b>&nbsp;&nbsp;<?= date('Y-m-d h:i A',Auth::user()->iLastLoginAt) ?></small>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('backend.home')}}" class="nav-link <?= ($routeArr[1] == 'home') ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                
                <li class="nav-item <?= ($routeArr[1] == 'content_page') ? 'menu-open' : '' ?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-database"></i>
                        <p>Master Data <i class="fas fa-angle-left right"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.content_page.index') }}" class="nav-link <?= ($routeArr[1] == 'content_page') ? 'active' : '' ?>">
                                <i class="fas fa-file-alt nav-icon"></i>
                                <p>Content Pages</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{route('backend.logs')}}" class="nav-link <?= ($routeArr[1] == 'logs') ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-history"></i>
                        <p>Logs</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <!-- /.sidebar -->
  </aside>