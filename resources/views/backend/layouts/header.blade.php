<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                {{-- <a href="{{ route('backend.profile') }}" class="dropdown-item">
                <i class="fas fa-user"></i> Profile
                </a> --}}
                <div class="dropdown-divider"></div>
                <a href="{{ route('backend.change_password') }}" class="dropdown-item">
                <i class="fas fa-lock"></i> Change password
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('backend.logout') }}" class="dropdown-item">
                    <i class="fas fa-power-off"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
  <!-- /.navbar -->