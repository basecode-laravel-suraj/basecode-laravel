<?php

return [
    'signup_success' => 'User registered successfully.',
    'email_already_exist' => 'Email address already registered with '.config('app.name').'. Try another email address.',
    'login_success' => 'User logged in successfully.',
    'login_user_not_found' => 'Sorry, You have entered wrong email or password.',
    'account_inactive' => 'Sorry, Your account deactivated by '.config('app.name').' admin team.',
    'invalid_social_type' => 'Invalid social account type.',
    'change_password_success' => 'Password successfully changed.',
    'incorrect_old_password' => 'Current password is wrong.',
    'logout_success' => 'Logout success.',
    'unauthenticated' => 'Your session is expired. Please sign in again.',
    'user_profile' => 'User profile information.',
    'profile_updated' => 'Profile updated successfully.',
    'content_page' => 'Content page.',
    'invalid_content_page' => 'Invalid content page.',
];