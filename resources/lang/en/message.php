<?php
return [
    'something_wrong' => 'Something went wrong. Please try again later.',
    'old_password_wrong' => 'The old password is wrong.',
    'password_changed' => 'Password successfully changed.',
    'post_liked' => ':name have liked on your :post post.',
];