<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('iUserId');
            $table->char('vUserUuid', 36)->nullable()->unique('vUserUuid');
            $table->string('vEmailId',100)->nullable();
            $table->string('vPassword',100)->nullable();
            $table->string('vName',50)->nullable(false);
            $table->string('vISDCode', 5)->nullable();
            $table->string('vMobileNumber', 15)->nullable();
            $table->text('vProfilePic')->nullable();
            $table->string('vLangISOCode', 5);
            $table->boolean('tiIsSocialLogin')->default(0)->comment('0 = No, 1 = Yes');
            $table->text('vPasswordResetToken')->nullable();
            $table->string('vTimezone', 5)->nullable();
            $table->integer('iOTP')->nullable();
            $table->integer('iOTPExpireAt')->nullable();
            $table->boolean('tiIsMobileVerified')->default(0)->comment('0 = No, 1 = Yes');
            $table->boolean('tiIsActive')->default(1)->comment('0 = No, 1 = Yes');
            $table->boolean('tiIsDeleted')->nullable()->default(0)->comment('0 = No, 1 = Yes');
            $table->integer('iCreatedAt')->nullable(false);
            $table->integer('iUpdatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
