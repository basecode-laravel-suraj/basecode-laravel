<?php

use App\Http\Controllers\Api\v1\OauthController;
use App\Http\Controllers\Api\v1\SettingController;
use App\Http\Controllers\Api\v1\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('v2')->as('api.')->group(function () {
    Route::get('content-pages/{vSlug}', [SettingController::class, 'contentPages']);
    Route::prefix('oauth')->group(function () {
        Route::post('signup', [OauthController::class, 'signup']);
        Route::post('login', [OauthController::class, 'login']);
        Route::post('social-signin', [OauthController::class, 'socialSignin']);
    });
    Route::middleware('auth:sanctum')->group(function () {
        Route::prefix('user')->group(function () {
            Route::get('logout', [UserController::class, 'logout']);
            Route::post('change-password', [UserController::class, 'changePassword']);
            Route::prefix('profile')->group(function () {
                Route::get('/', [UserController::class, 'profile']);
                Route::post('/', [UserController::class, 'editProfile']);    
            });
        });
    });
});