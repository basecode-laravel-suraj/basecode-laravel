<?php

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\ContentPageController;
use App\Http\Controllers\Backend\HomeController;
use Illuminate\Support\Facades\Route;

Route::prefix('')->as('backend.')->group(function () {
    Route::get('login', [AdminController::class,'login'])->name('login');
    Route::post('dologin', [AdminController::class,'dologin'])->name('dologin');
    
    Route::middleware('auth:backend')->group(function () {
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');
        Route::get('', [HomeController::class, 'index'])->name('home');
        Route::get('logout', [AdminController::class,'logout'])->name('logout');
        Route::any('change_password', [AdminController::class,'ChangePassword'])->name('change_password');
        //Static content
        Route::resource('content_page', ContentPageController::class,['only' => ['index', 'show', 'edit', 'update']]);
    });



});