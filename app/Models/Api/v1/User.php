<?php

namespace App\Models\Api\v1;

use App\Models\User as Authenticatable;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    protected $primaryKey = 'iUserId';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vUserUuid','vEmailId','vPassword','vName','vISDCode','vMobileNumber','vProfilePic','iCreatedAt','iUpdatedAt'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'iUserId','vTimezone','tiIsSocialLogin','iOTP','iOTPExpireAt','tiIsMobileVerified','tiIsActive','tiIsDeleted'
    ];

    protected $attributes = [
        'vLangISOCode' => 'en',
        'tiIsSocialLogin' => 0,
        'tiIsMobileVerified' => 0,
        'tiIsActive' => 1,
        'tiIsDeleted' => 0
    ];

    //Signup
    public function signup($request) {
        if (!$this->IsEmailRegistered($request->vEmailId)) {
            try {
                DB::beginTransaction();
                $user = new User();
                $user->fill($request->all());
                if ($request->hasFile('vProfilePic')) {
                    $request->file('vProfilePic')->store('public/uploads');
                    $file_name = $request->file('vProfilePic')->hashName();
                    $user->vProfilePic = $file_name;
                }
                $user->vUserUuid = GetUuid();
                $user->vPassword = Hash::make($request->vPassword);
                $user->iCreatedAt = time();
                $user->save();
                $user_data = $this->GetUserData($user,true);
                $this->CreateOrUpdateDevice($user->iUserId,$user_data['vAccessToken'],$request);
                DB::commit();
                return SuccessResponseWithResult($user_data,"api.signup_success");
            } catch (\Exception $ex) {
                DB::rollback();
                return ExceptionResponse($ex);
            }
        } else {
            return ErrorResponse('api.email_already_exist');;
        }
    }

    //Validate email address
    private function IsEmailRegistered($vEmailId) {
        $email_result = User::select("vUserUuid")
            ->whereRaw("LOWER(vEmailId) = '".strtolower($vEmailId)."' AND tiIsDeleted = 0")
            ->first();
        
        return (!empty($email_result)) ? true : false;
    }

    private function CreateOrUpdateDevice($iUserId,$vAccessToken,$request) {
        $device = Device::where("iUserId",$iUserId)->first();
        if(!empty($device)) {
            $device->vAccessToken = $vAccessToken;
            $device->vDeviceToken = $request->vDeviceToken;
            $device->tiDeviceType = $request->tiDeviceType;
            $device->vDeviceName = $request->vDeviceName ?? "";
            $device->iUpdatedAt = time();
            $device->update();
        } else {
            $model = new Device();
            $model->vAccessToken = $vAccessToken;
            $model->iUserId = $iUserId;
            $model->vDeviceToken = $request->vDeviceToken;
            $model->tiDeviceType = $request->tiDeviceType;
            $model->vDeviceName = $request->vDeviceName ?? "";
            $model->iCreatedAt = time();
            $model->save();
        }
    }

    private function GetUserData($user, $flag = false) {
        $vProfilePic = "";
        if($user->tiIsSocialLogin == 1) {
            $vProfilePic = $user->vProfilePic;
        } else {
            $vProfilePic = (!empty($user->vProfilePic)) ? url(Storage::url("uploads/".$user->vProfilePic)) : "";
        }
        $result = [
            "vUserUuid" => $user->vUserUuid,
            "vName" => $user->vName,
            "vEmailId" => $user->vEmailId,
            "vISDCode" => $user->vISDCode ?? "",
            "vMobileNumber" => $user->vMobileNumber ?? "",
            "tiIsSocialLogin" => $user->tiIsSocialLogin,
            "vProfilePic" => $vProfilePic,
            "vAccessToken" => ($flag) ? $user->createToken(config('app.app_secret'))->plainTextToken : request()->bearerToken(),
        ];
        return $result;
    }

    //Login
    public function login($request) {
        try {
            $user = User::where(["vEmailId" => $request->vEmailId,"tiIsDeleted" => 0])->first();
            if (!empty($user)) {
                if (!Hash::check($request->vPassword, $user->vPassword)) {
                    return ErrorResponse('api.login_user_not_found');    
                }
            } else {
                return ErrorResponse('api.login_user_not_found');
            }
            $user->tokens()->delete();
            if($user->tiIsActive != 1) {
                return ErrorResponse('api.account_inactive');
            }
            $user->update();
            $user_data = $this->GetUserData($user,true);
            $this->CreateOrUpdateDevice($user->iUserId,$user_data['vAccessToken'],$request);

            return SuccessResponseWithResult($user_data,"api.login_success");
        } catch (\Exception $ex) {
            return ExceptionResponse($ex);
        }
    }

    //Social signin
    public function socialSignin($request) {
        $social_user = GetSocialId($request->tiSocialType, $request->vSocialId);

        if($social_user['responseCode'] == 200) {
            $data = $social_user['responseData'];

            $user = User::from("users as u")->select("u.*")
                        ->join("social_accounts as s","u.iUserId","=","s.iUserId")
                        ->where(["s.vSocialId" => $data['vSocialId'],"s.tiSocialType" => $data['tiSocialType'],"u.tiIsSocialLogin" => 1,"tiIsDeleted" => 0])
                        ->first();
            if(empty($user)) {
                if($user->tiIsActive != 1) {
                    return ErrorResponse('api.account_inactive');
                }
                DB::beginTransaction();
                $model = new User();
                $model->vUserUuid = GetUuid();
                $model->vEmailId = $data["vEmailId"] ?? "";
                $model->vName = $data["vName"];
                $model->vProfilePic = $data['vImageUrl'];
                $model->tiIsSocialLogin = 1;
                $model->vTimezone = $request->vTimezone ?? NULL;
                $model->iCreatedAt = time();
                $model->iUpdatedAt = time();
                $model->save();

                $social_model = new SocialAccount();
                $social_model->iUserId = $model->iUserId;
                $social_model->vSocialId = $data['vSocialId'];
                $social_model->tiSocialType = $data['tiSocialType'];
                $social_model->vImageUrl = $data['vImageUrl'];
                $social_model->iCreatedAt = time();
                $social_model->iUpdatedAt = time();
                $social_model->save();

                $user_data = $this->GetUserData($model,true);
                $this->CreateOrUpdateDevice($model->iUserId,$user_data['vAccessToken'],$request);
                DB::commit();
                return SuccessResponseWithResult($user_data,"api.login_success");
            } else {
                DB::beginTransaction();
                User::where('iUserId',$user->iUserId)->update([
                    'vEmailId' => $data["vEmailId"] ?? "",
                    'vName' => $data["vName"],
                    'vProfilePic' => $data['vImageUrl'],
                    'tiIsSocialLogin' => 1,
                    'vTimezone' => $request->vTimezone ?? NULL,
                    'iUpdatedAt' => time()
                ]);

                SocialAccount::where('iUserId',$user->iUserId)->update([
                    'vImageUrl' => $data['vImageUrl'],
                    'iUpdatedAt' => time()
                ]);
                $user_data = $this->GetUserData($user,true);
                $this->CreateOrUpdateDevice($user->iUserId,$user_data['vAccessToken'],$request);
                DB::commit();
                return SuccessResponseWithResult($user_data,"api.login_success");
            }
        } else {
            return $social_user;
        }
    }

    //Change password
    public function changePassword($request) {
        try {
            $user = Auth::user();
            if (Hash::check($request->vCurrentPassword, $user->vPassword)) {
                User::where('iUserId', $user->iUserId)
                        ->update([
                    'vPassword' => Hash::make($request->vNewPassword),
                ]);
                return SuccessResponse('api.change_password_success');
            } else {
                return ErrorResponse('api.incorrect_old_password');
            }
        } catch (\Exception $ex) {
            return ExceptionResponse($ex);
        }
    }

    //Logout
    public function logout() {
        try {
            $user = Auth::user();
            Device::where(['iUserId' => $user->iUserId])->delete();
            $user->tokens()->delete();
            return SuccessResponse('api.logout_success');
        } catch (\Exception $ex) {
            return ExceptionResponse($ex);
        }
    }

    //User profile
    public function profile() {
        $user = Auth::user();
        $user_data = $this->GetUserData($user);        
        return SuccessResponseWithResult($user_data,"api.user_profile");
    }

    //Edit Profile
    public function editProfile($request) {
        try {
            $user = Auth::user();
            if ($request->hasFile('vProfilePic')) {
                $request->file('vProfilePic')->store('public/uploads');
                $file_name = $request->file('vProfilePic')->hashName();
                $user->vProfilePic = $file_name;
            }
            $user->vName = $request->vName;
            $user->iUpdatedAt = time();
            $user->save();
            return SuccessResponseWithResult($this->GetUserData($user),"api.profile_updated");
        } catch (\Exception $ex) {
            return ExceptionResponse($ex);
        }
    }
}
