<?php
/**
 * @OA\Get(
 *      path="/user/logout",
 *      tags={USER_TAG},
 *      summary="SignOut User",
 *      operationId="signOut",
 *      security={{ "bearerAuth":{} }},
 *      @OA\Response(
 *         response=200,
 *         description="success",
 *         @OA\JsonContent(ref="#/components/schemas/CommonFields"),
 *      ),
 * )
 */
