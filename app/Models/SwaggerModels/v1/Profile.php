<?php
/**
 * @OA\Get(
 *      path="/user/profile",
 *      tags={USER_TAG},
 *      summary="User profile",
 *      operationId="userProfile",
 *      security={{ "bearerAuth":{} }},
 *      @OA\Response(
 *         response=200,
 *         description="success",
 *         @OA\JsonContent(ref="#/components/schemas/UserResponse"),
 *      ),
 *      @OA\Response(
 *         response=400,
 *         description="Error",
 *         @OA\JsonContent(ref="#/components/schemas/CommonFields"),
 *      )
 * )
 */