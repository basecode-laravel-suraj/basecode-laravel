<?php
/**
 * @OA\Post(
 *      path="/user/profile",
 *      tags={USER_TAG},
 *      summary="Edit user profile",
 *      operationId="editProfile",
 *      security={{ "bearerAuth":{} }},
 *      @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 required={"vName"},
 *                 @OA\Property(property="vName", type="string"),
 *                 @OA\Property(
 *                     property="vProfilePic",
 *                     type="file"
 *                 )
 *             )
 *         )
 *      ),
 *      @OA\Response(
 *         response=200,
 *         description="success",
 *         @OA\JsonContent(ref="#/components/schemas/UserResponse"),
 *      ),
 *      @OA\Response(
 *         response=400,
 *         description="Error",
 *         @OA\JsonContent(ref="#/components/schemas/CommonFields"),
 *      )
 * )
 */