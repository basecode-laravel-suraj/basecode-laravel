<?php
/**
 * @OA\Post(
 *      path="/oauth/login",
 *      tags={AUTHENTICATION_TAG},
 *      summary="Login User",
 *      operationId="signIn",
 *      @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 required={"vEmailId", "vPassword", "vDeviceToken","tiDeviceType"},
 *                 @OA\Property(property="vEmailId", type="string"),
 *                 @OA\Property(property="vPassword", type="string"),
 *                 @OA\Property(property="vDeviceToken", type="string"),
 *                 @OA\Property(property="tiDeviceType", type="integer", format="int32", default="1",enum={0,1,2},description="0 = Web, 1 = android, 2 = ios"),
 *                 @OA\Property(property="vDeviceName", type="string")
 *             )
 *         )
 *      ),
 *      @OA\Response(
 *         response=200,
 *         description="success",
 *         @OA\JsonContent(ref="#/components/schemas/UserResponse"),
 *      ),
 *      @OA\Response(
 *         response=400,
 *         description="Error",
 *         @OA\JsonContent(ref="#/components/schemas/CommonFields"),
 *      )
 * )
 */

 /**
 * @OA\Schema(
 *   schema="UserResponse",
 *   type="object",
 *   allOf={
 *     @OA\Schema(ref="#/components/schemas/CommonFields"),
 *     @OA\Schema(
 *          @OA\Property(
 *              property="responseData", 
 *              type="object",
 *              ref="#/components/schemas/UserResponseFields"
 *          )
 *      )
 *   }
 * )
 */

/**
 * @OA\Schema(
 *   schema="UserResponseFields",
 *   type="object",
 *      @OA\Property(
 *          property="vUserUuid", 
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="vName", 
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="vEmailId", 
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="vISDCode", 
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="vMobileNumber", 
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="tiIsSocialLogin", 
 *          type="integer",
 *          format="int16"
 *      ),
 *      @OA\Property(
 *          property="vProfilePic", 
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="vAccessToken", 
 *          type="string"
 *      )
 * )
 */