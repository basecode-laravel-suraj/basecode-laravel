<?php

namespace App\Http\Controllers\Backend;

use App\Common;
use App\Models\Backend\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;

class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        if (!Auth::guard('backend')->check()) {
            $model = new Admin();
            return view('backend.admin.login',compact('model'));
        }

        return redirect()->guest(route('backend.home'));
    }

    public function dologin(Request $request) {
        // Validate form data
        $this->validate($request, [
            'vEmail' => 'required|email',
            'vPassword' => 'required',
            'iTimezoneOffset' => 'required|integer'
        ]);

        $remember_me = $request->has('remember_me') ? true : false; 
        $isLogin = Auth::guard('backend')->attempt([
            'vEmail' => $request->vEmail, 
            'password' => $request->vPassword,
            'tiIsDeleted' => 0
        ],$remember_me);
        if ($isLogin) {
            $admin = Auth::guard('backend')->user();
            if($admin->tiIsActive == 1) {
                DB::update('update admins set iLastLoginAt = ? where iAdminId = ?', [time(),$admin->iAdminId]);
                return redirect()->guest(route('backend.home'));
            } else {
                Auth::logout();
                $request->session()->flush();
                $request->session()->flash('error', 'Your account is inactive');
                return redirect()->back()->withInput($request->only('vEmail'));
            }
        }
        // Authentication failed, redirect back to the login form
        $request->session()->flash('error', 'Wrong email or password.');
        return redirect()->back()->withInput($request->only('vEmail'));
    }

    public function logout(Request $request) {
        Auth::logout();

        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('backend.login'));
    }

    public function error() {
        return view('backend.admin.error');
    }

    //Change password
    public function ChangePassword(Request $request,MessageBag $message_bag) {
        $user = Auth::user();
        if($request->isMethod("post")) {
            // Validate form data
            $this->validate($request, [
                'vCurrentPassword' => 'required',
                'vNewPassword' => 'required|min:8|different:vCurrentPassword',
                'vConfirmPassword' => 'required_with:vNewPassword|same:vNewPassword',
            ]);

            if (!Hash::check($request->vCurrentPassword, $user->vPassword)) { 
                $message_bag->add('vCurrentPassword', __("message.old_password_wrong"));
                return Redirect::back()->withInput($request->all())->withErrors($message_bag);
            }

            $user->vPassword = Hash::make($request->vNewPassword);
            $user->save();

            FormSuccessResponse($request,"message.password_changed");
            return redirect()->route('backend.change_password');
        }
        return view('backend.admin.changepassword');
    }

    //Profile
    public function Profile(Request $request,MessageBag $message_bag) {
        $model = Auth::user();
        $validation_rules = $model->Validate("profile");
        if($request->isMethod("post")) {
            $this->ValidateForm($request,$validation_rules);
            $time = time();
            $model->vFirstName = $request->vFirstName;
            $model->vLastName = $request->vLastName;
            $model->iUpdatedAt = $time;
            $model->save();

            FormSuccessResponse($request,"message.profile_updated");
            return redirect()->route('backend.profile');
        }

        return view('backend.admin.profile',compact('model','validation_rules'));
    }
}