<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\ContentPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContentPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ContentPage::select("vPageUuid","vPageName","vSlug","iUpdatedAt")->get();
        return view('backend.content_page.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $model = $this->getPage($uuid);
        return view('backend.content_page.show',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $model = $this->getPage($uuid);
        return view('backend.content_page.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $this->validate($request, [
            'vPageName' => 'required|string',
            'txContent' => 'required',
                ], [
            'vPageName.required' => 'The page name field is required.',
            'txContent.required' => 'The content field is required.',
        ]);
        try {
            $model = $this->getPage($uuid);
            $model->vPageName = $request->vPageName;
            $model->txContent = $request->txContent;
            $model->iUpdatedAt = time();
            if ($model->update()) {
                $request->session()->flash('success', $request->vPageName.' content updated successfully.');
            } else {
                $request->session()->flash('error', 'Fail to update '.$request->vTitle.' content.');
            }
            return redirect()->route('backend.content_page.show', ['content_page' => $uuid]);
        } catch (\Exception $ex) {
            $request->session()->flash('error', __('message.something_wrong'));
            return redirect()->route('backend.content_page.edit', ['content_page' => $uuid]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Get page detail
    private function getPage($uuid) {
        $model = ContentPage::where(['vPageUuid' => $uuid])->first();
        if(empty($model)) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
