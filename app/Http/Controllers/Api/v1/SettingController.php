<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends BaseController
{
    public function contentPages($vSlug) {
        
        $result = DB::table("content_pages")
                    ->selectRaw("vPageName,txContent,IFNULL(iUpdatedAt,'') as iUpdatedAt")
                    ->where(["vSlug" => $vSlug,"tiIsActive" => 1])->first();
        
        if(!empty($result)) {
            return SuccessResponseWithResult($result,"api.content_page");
        } else {
            return ErrorResponse("api.invalid_content_page");
        }
    }
}
