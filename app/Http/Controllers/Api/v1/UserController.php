<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Api\v1\User;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    //Change password
    public function changePassword(Request $request) {
        $rules = [
            'vCurrentPassword' => 'required',
            'vNewPassword' => 'required|min:8|different:vCurrentPassword',
            'vConfirmPassword' => 'required_with:vNewPassword|same:vNewPassword',
        ];

        if($this->ApiValidator($request->all(), $rules)){
            return $this->SendResponse($this->response);
        }
        
        $model = new User();
        $response = $model->changePassword($request);
        return $this->SendResponse($response);
    }

    //Logout
    public function logout() {
        $model = new User();
        $response = $model->logout();
        return $this->SendResponse($response);
    }

    //Profile
    public function profile() {
        $model = new User();
        $response = $model->profile();
        return $this->SendResponse($response);
    }

    //Edit profile
    public function editProfile(Request $request) {
        $rules = [
            'vName' => 'required',
            'vProfilePic' => 'nullable|image',
        ];

        if($this->ApiValidator($request->all(), $rules)){
            return $this->SendResponse($this->response);
        }
        
        $model = new User();
        $response = $model->editProfile($request);
        return $this->SendResponse($response);
    }
}
