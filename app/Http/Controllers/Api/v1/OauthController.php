<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Api\v1\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class OauthController extends BaseController
{
    public function signup(Request $request) {
        $rules = [
            'vName' => 'required',
            'vEmailId' => 'required|email',
            'vPassword' => 'nullable|required_if:tiSignupType,==,1|min:8',
            'vDeviceToken' => 'required',
            'tiDeviceType' => 'required|in:1,2',
            'vProfilePic' => 'nullable|image',
        ];

        if($this->ApiValidator($request->all(), $rules)){
            return $this->SendResponse($this->response);
        }
        
        $model = new User();
        $response = $model->signup($request);
        return $this->SendResponse($response);
    }

    //Login
    public function login(Request $request) {
        $rules = [
            'vEmailId' => 'required|email',
            'vPassword' => 'required',
            'vDeviceToken' => 'required',
            'tiDeviceType' => 'required|in:1,2'
        ];
        $messages = [];
        if($this->ApiValidator($request->all(), $rules,$messages)){
            return $this->SendResponse($this->response);
        }
        $model = new User();
        $response = $model->login($request);
        return $this->SendResponse($response);
    }

    //Social signin
    public function socialSignin(Request $request) {
        $rules = [
            'vSocialId' => 'required',
            'tiSocialType' => 'required|in:1,2',
            'tiDeviceType' => 'required|in:1,2',
            'vDeviceToken' => 'required'
        ];
        $messages = [];
        if($this->ApiValidator($request->all(), $rules,$messages)){
            return $this->SendResponse($this->response);
        }
        $model = new User();
        $response = $model->socialSignin($request);
        return $this->SendResponse($response);
    }
}
