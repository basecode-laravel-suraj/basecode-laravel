<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

//Generate random unique string
function GenerateRandomString($length = 0)
{
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime() * 1000000);
    $i = 0;
    $pass = '';
    $length = ($length > 0) ? $length : 128;
    while ($i <= $length) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

function BackgroundProcess($url, $postfields)
{
    $ch = curl_init($url);

    curl_setopt_array($ch, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_NOSIGNAL => 1, //to timeout immediately if the value is < 1000 ms
        CURLOPT_TIMEOUT_MS => 100, //The maximum number of mseconds to allow cURL functions to execute
        CURLOPT_VERBOSE => 1,
        CURLOPT_POSTFIELDS => $postfields,
        CURLOPT_HEADER => 1,
        CURLOPT_FRESH_CONNECT => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false
    ));

    curl_exec($ch);

    curl_close($ch);
    return true;
}

function TimeElapsedString($datetime, $full = false)
{
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function GetUuid() {
    return Uuid::uuid6();
}

function ExceptionResponse($ex) {
    $message = $ex->getMessage();
    if(!App::environment('production')) {
        $message = $message." (Path: ".$ex->getFile()." : Line no ".$ex->getLine().")";
        Log::error($message);
    }
    return [
        'responseCode' => 400,
        'responseMessage' => $message
    ];
}

function ErrorResponse($message_key,$placeholders = []) {
    return [
        'responseCode' => 400,
        'responseMessage' => __($message_key,$placeholders)
    ];
}

function SuccessResponse($message_key,$placeholders = []) {
    return [
        'responseCode' => 200,
        'responseMessage' => __($message_key,$placeholders)
    ];
}

function SuccessResponseWithResult($result,$message_key,$placeholders = []) {
    return [
        'responseCode' => 200,
        'responseMessage' => __($message_key,$placeholders),
        'responseData' => $result
    ];
}

function FormSuccessResponse($request,$message_key,$placeholders = []) {
    $request->session()->flash('success', __($message_key,$placeholders));
}

function FormErrorResponse($request,$message_key,$placeholders = []) {
    $request->session()->flash('error', __($message_key,$placeholders));
}

function GetSocialId($tiSocialType, $vSocialId) {
    try {
        if ($tiSocialType == 1) { //Facebook
            $result = SendCurlRequest(env('FB_URL') . '?access_token=' . $vSocialId . '&fields=' . env('FB_RETURN_FIELDS'));
            if (!isset($result->error)) {
                $params = [
                    'vSocialId' => $result->id,
                    'vName' => $result->first_name ?? NULL . '' . $result->last_name ?? NULL,
                    'vEmailId' => $result->email ?? NULL,
                    'tiSocialType' => 1,
                    'vImageUrl' => $result->picture->data->url,
                ];
                return SuccessResponseWithResult($params,"Social login");
            } else {
                return ErrorResponse($result->error->message);
            }
        } else if ($tiSocialType == 2) { //Google
            $result = SendCurlRequest(env('GOOGLE_URL') . '&access_token=' . $vSocialId);
            if (!isset($result->error)) {
                $resource = explode('/', $result->resourceName);
                $params = [
                    'vSocialId' => $resource[1],
                    'vName' => $result->names[0]->givenName ?? NULL . ' ' . $result->names[0]->familyName ?? NULL,
                    'vEmailId' => !empty($result->emailAddresses[0]->value) ? $result->emailAddresses[0]->value : "",
                    'tiSocialType' => 2,
                    'vImageUrl' => $result->photos[0]->url,
                ];
                return SuccessResponseWithResult($params,"Social login");
            } else {
                return ErrorResponse($result->error->message);
            }
        } else {
            return ErrorResponse("api.invalid_social_type");
        }
    } catch (Exception $ex) {
        return ExceptionResponse($ex);
    }
}

function SendCurlRequest($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result = json_decode($result);
}